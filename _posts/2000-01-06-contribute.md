---
title: "contribute"
bg: yellow
color: black
fa-icon: group
---

Do you want to **contribute**? Here you have some ways if you want to help:

* [Report **bugs**](https://gitorious.org/projects/luporum/issues)
* Write **patches** and: 
  * attach them to the reported bugs or
  * make a merge request or
  * send it to me via email
* **Develop** a client for lupus. Let me know of them and we can add them to this web!
* **Contact** me at: [monica@probeta.net](mailto:monica@probeta.net)