---
title: "lupus"
bg: green
color: black
fa-icon: cloud
---

**lupus** is a REST API that configures an UCI sytem remotely via HTTP requests.

[API documentation](lupus-doc.html)

**lupus** has different modules:

#### uci

This module tries to emulate the [uci command line](http://wiki.openwrt.org/doc/uci#command.line.utility). 
You can:

* Add and delete sections.
* Get the type of a section.
* Add and delete options.
* Get the value of an option.
* Set a values of an option.
* Get all sections and options of a config.
* Get all configs with their sections and options.
* Get file names of /etc/config.

#### system

This module has some utilities to interact with the system to be able to reload 
configurations. You can:

* Start, restart and stop a service.
* Reboot the system

#### authentication

To configure the UCI system, clients should use an authentication method. 
With this module you can set up the authentication you want to use.